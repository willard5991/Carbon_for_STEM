#Carbon for STEM project

- driver.py : A web scraper designed to download images from bugguide.net for use in machine learning
- clean_names.py : a script that cleans up any weirdness in the file or directory names after they were downloaded
- condense_images.py : a script that creates a .txt file of image names and their labels/directory location
- insect_cnn.ipynb : an IPythonNotebook that classifies images that were downloaded using the above scraper

More to be added...