import requests
import bs4
import os
import time
import random
import sys
import pickle

base_url = 'http://bugguide.net/node/view/878075/tree/all'
base_dir = os.path.join(os.path.split(os.getcwd())[0],"images")
base_dir = base_dir.replace('My Documents','Documents')

def request(url,req_delay=9):
    time.sleep(req_delay)
    return requests.get(url)

def get_links():
    print 'getting links...'
    sys.stdout.flush()
    site = request(base_url)
    soup = bs4.BeautifulSoup(site.content)
    
    links = []
    ital = soup.find_all('i')
    for e in ital:
        if not e.text[0].isupper():
            links.append(e.find('a')['href'])
    pickle.dump(links,open(os.path.join(base_dir,'site_links.pickle'),'wb'))
    pickle.dump(links,open(os.path.join(base_dir,'site_links_backup.pickle'),'wb'))
    return links

def scrape():
    #either generate the list of links or load it from a pickle file
    links = []
    if not os.path.isfile(os.path.join(base_dir,'site_links.pickle')):
        links = get_links()
    else:
        links = pickle.load(open(os.path.join(base_dir,'site_links.pickle'),'rb'))
    #links = get_links() if not os.path.exists(os.path.join(base_dir,'site_links.pickle')) else pickle.load(open(os.path.join(base_dir,'site_links.pickle'),'rb'))
    
    num_images_downloaded = 0
    num_images_skipped = 0
    num_species = len(links)
    for m in range(num_species):
        l = links.pop(0) #remove first link from list
        site = request(l)
        soup = bs4.BeautifulSoup(site.content)

        """ For each page, goes in and grabs the entire taxonomy structure.
        This will be used to create the directory structure for scraping
        the images into. """
        dirs = []
        for taxon in soup.find(class_='bgpage-classification').find_all('a'):
            dirs.append(taxon.text)
        #create directories based on taxonomy for putting images into
        file_path = os.path.join(base_dir,*dirs)
        if not os.path.exists(file_path):
            if '\"' in file_path or '\'' in file_path or '.' in file_path:
                file_path = file_path.replace('\"','')
                file_path = file_path.replace('\'','')
                file_path = file_path.replace('.','')
            os.makedirs(file_path)

        l = l[:l.rfind('/')+1]+'bgimage' #change to the Images tab
        site = request(l)
        soup = bs4.BeautifulSoup(site.content)
        #grab thumbnails rather than full pictures
        images = soup.find_all(class_='bgimage-thumb')
        #print images,'\n\n'
        #print len(images),'\n\n'
        '''
        #grab full pictures rather than thumbnails
        nma = soup.find(class_='node-main-alt')
        image_links = nma.find_all('a')[1:]
        images = []
        for il in image_links:
            imag = request(il['href'])
            imag_soup = bs4.BeautifulSoup(imag.content)
            images.append(imag_soup.find(class_='bgimage-image')['src'])
        '''
        im_num = 0
        for im in images:
            im_num += 1
            #next line only needed for thumbnails
            im = im['src']
            time.sleep(9)
            im_site = requests.get(im)
            file_name = dirs[-1]+str(im_num)
            if ('\"' in file_name) or ('\'' in file_name) or '.' in file_name:
                file_name = file_name.replace('\"','')
                file_name = file_name.replace('\'','')
                file_name = file_name.replace('.','')
            file_name = file_name +'.jpg'#im[im.rfind('/')+1:]
            print file_name
            try:
                with open(os.path.join(file_path,file_name),'wb') as f:
                    f.write(im_site.content)
                    #output progress (how many images downloaded)
                    num_images_downloaded+=1
            except IOError:
                print 'Error downloading image %d from species %s' %(im_num,dirs[-1])
                num_images_skipped += 1
            print '\rSpecies:', dirs[-1],'| Spec. Progress: ',str(m+1)+'/'+str(num_species),'| Total Downloaded:',num_images_downloaded, '| Skipped: ', num_images_skipped,
            sys.stdout.flush()
        print '| Continue?',
        sys.stdout.flush()
        pause_break = raw_input()
        if pause_break == 'n' or pause_break == 'N':
            if links != []: #check to see if there are any links left
                #save updated list of links to a pickle so work can be picked back up at a later point
                pickle.dump(links,open(os.path.join(base_dir,'site_links.pickle'),'wb'))
                time.sleep(1) #give it time to make sure nothing gets interrupted by the exit next
            sys.exit(0)
        print '\033[F'
        sys.stdout.flush()        

scrape()
