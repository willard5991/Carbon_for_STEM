import os

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

#some weirdness going on in this processing with directories that are renamed. however, should be good enough to work with, can try to refine at a later point if needed

base_dir = os.getcwd()

print(base_dir)

for path,dirs,files in os.walk(base_dir):
    for d in dirs:
        if ' ' in d:
            print(path,d)
            os.rename(os.path.join(path,d),os.path.join(path,d.replace(' ','_')))
            d = d.replace(' ','_')
        if '.' in d:
            print(path,d)
            os.rename(os.path.join(path,d),os.path.join(path,d.replace('.','')))
    for f in files:
        if ' ' in f:
            os.rename(os.path.join(path,f),os.path.join(path,f.replace(' ','_')))
            f = f.replace(' ','_')
        f1 = f[:f.rfind('.')]
        file_ending = f[f.rfind('.'):]
        if '.' in f1:
            f1 = f1.replace('.','') + file_ending
            os.rename(os.path.join(path,f),os.path.join(path,f1))