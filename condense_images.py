import os

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

base_dir = os.getcwd()
file_name = "image_info.txt"

def path_to_list(path,start_at=''):
    if start_at != '':
        path = path[path.find(start_at):]
    path_list = path.split(os.path.sep)
    return path_list if path_list[0] != '' else path_list[1:] #remove extra '' entry for unix style paths

with open(file_name,'w') as f:
    for path,dirs,files in os.walk(base_dir):
        if files != []:
            for f1 in files:
                f.write(str(path_to_list(path,start_at='Arthropoda')).replace(' ','').replace('\'','')+','+f1+'\n')
